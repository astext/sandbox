import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

declare var require: any;



@Component({
  selector: 'app-diagnostics',
  templateUrl: './diagnostics.component.html',
  styleUrls: ['./diagnostics.component.scss'],
  encapsulation:ViewEncapsulation.Emulated
})
export class DiagnosticsComponent implements OnInit {

  
  appver = require("../../../package.json").version;
  bootstrapver = require("../../../node_modules/bootstrap/package.json").version;
  angularver = require("../../../node_modules/@angular/core/package.json")._id;
  ngcliver = require("../../../node_modules/@angular/compiler-cli/package.json")._id;
  typescriptver = require("../../../node_modules/typescript/package.json")._id;
  fontawesomever = require("../../../node_modules/@fortawesome/fontawesome-free/package.json")._id;

  lastUpdate = new Date(document.lastModified);

  



  constructor() { }

  ngOnInit(): void {
  }

}
