import { Input, Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appDiarycard]'
})


export class DiarycardDirective {

  constructor( private elementRef: ElementRef ) { }

  ngOnInit() {
    this.elementRef.nativeElement.style.borderRadius = '20px';
    
  }

  // https://medium.com/javascript-in-plain-english/using-built-in-angular-directives-849c4e7fd882

  //https://www.techiediaries.com/angular-elementref/

}
